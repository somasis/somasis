# Copyright 2014-2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2#

require launchpad
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require vala [ vala_dep=true with_opt=true option_name=gobject-introspection ]
require systemd-service

SUMMARY="Application matching framework"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="gobject-introspection gtk-doc"

DEPENDENCIES="
    build:
        dev-libs/libxml2[python][python_abis:2.7]
        dev-libs/libxslt[python][python_abis:2.7]
        gobject-introspection? ( gnome-desktop/gobject-introspection[>=0.10.2] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
        virtual/pkg-config
    build+run:
        dev-libs/glib[>=2.32.0]
        dev-libs/libdbusmenu:0.4
        gnome-desktop/libgtop:2
        gnome-desktop/libwnck:3.0[>=3.4.7]
        x11-libs/libX11
        x11-libs/startup-notification
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-export-actions-menu
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    gtk-doc
)

DEFAULT_SRC_INSTALL_PARAMS=(
    systemddir=${SYSTEMDUSERUNITDIR}
)

src_prepare() {
    edo sed \
        -e 's/ -Werror/ /' \
        -e "s#pkg-config#${PKG_CONFIG}#g" \
        -e 's:python:python2:g' \
        -e 's/\$PYTHON/python2/' \
        -e 's/GNOME_COMMON_INIT/dnl &/' \
        -e 's/GNOME_COMPILE_WARNINGS/dnl &/' \
        -i configure.ac
    edo sed \
        -e 's|^#!.*|#!/usr/host/bin/python2|' \
        -i tests/gtester2xunit.py

    autotools_src_prepare
}

