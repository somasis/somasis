# Copyright 2015-2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=sahib tag=v${PV//_/-} ] scons

SUMMARY="Extremely fast tool to remove duplicates and other lint from your filesystem"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: elfutils libelf ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3] [[ note = [ for lib/formats/py.py ] ]]
        dev-python/Sphinx
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.32]
    run:
        core/json-glib
        sys-apps/util-linux
        providers:elfutils? ( dev-util/elfutils )
        providers:libelf? ( dev-libs/libelf )
"

SCONS_SRC_CONFIGURE_PARAMS=(
    --actual-prefix=/usr
    --prefix="${IMAGE}"/usr
    --libdir=lib
    --with-blkid
    --with-gettext
    --with-json-glib
    --with-libelf
    --without-gui
)

SCONS_SRC_INSTALL_PARAMS=(
    --actual-prefix=/usr
    --prefix="${IMAGE}"/usr
    --libdir=lib
    --with-blkid
    --with-gettext
    --with-json-glib
    --with-libelf
    --without-gui
)

src_prepare() {
    edo mkdir shim
    for t in cc; do
        edo ln -s $(type -fPp $(exhost --tool-prefix)"${t}") shim/"$t"
    done
    export PATH="${WORK}/shim:${PATH}"

    default
}

src_install() {
    scons_src_install

    edo gunzip "${IMAGE}"/usr/share/man/man1/rmlint.1.gz

    edo mkdir "${IMAGE}"/usr/$(exhost --target)
    edo mv "${IMAGE}"/usr/{,$(exhost --target)/}bin

    emagicdocs
}

